﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dogs.Models;

namespace Dogs.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Details()
        {
            Employee emp = new Employee() {
                EmpID = 1,
                Name="John",
                Gender = "Male",
                City = "Cairo"

            };

            return View(emp);
        }
    }
}