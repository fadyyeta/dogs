﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dogs.Models
{
    public class Employee
    {
        public int EmpID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
    }
}