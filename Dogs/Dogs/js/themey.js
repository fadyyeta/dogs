﻿

var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];


var gptAdSlots = [];

googletag.cmd.push(function () {

    var mappingHeader = googletag.sizeMapping().
    addSize([992, 0], [[970, 250], [970, 90], [728, 90], [468, 60]]).
    addSize([768, 0], [[728, 90], [468, 60], [320, 50]]).
    addSize([0, 0], []).
    build();

    var slot1 = googletag.defineSlot('/1749970/P4H_Responsive_Header_DFP', [728, 90], 'div-gpt-ad-1456153987748-0').addService(googletag.pubads());
    slot1.setCollapseEmptyDiv(true, true);
    slot1.defineSizeMapping(mappingHeader);








    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();

});

