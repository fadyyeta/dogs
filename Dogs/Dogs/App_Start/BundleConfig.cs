﻿using System.Web;
using System.Web.Optimization;

namespace Dogs
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/js/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/js/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/js/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/js/Scripts/bootstrap.js",
                      "~/js/Scripts/respond.js",
                      "~/js/jquery.mobile.custom.min.js",
                      "~/js/touch.js",
                      "~/js/typeahead.bundle.min.js",
                      "~/js/gpt.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/styles/bootstrap.min.css",
                      "~/Content/site.css",
                      "~/styles/customstyles3c77.css",
                      "~/styles/responsive-slider5d09.css",
                      "~/styles/font-awesome.min.css",
                      "~/styles/myStyles.css",
                      "~/styles/customstyles3c77.css"));
        }
    }
}
